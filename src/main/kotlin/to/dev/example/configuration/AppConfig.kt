package to.dev.example.configuration

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration

class AppConfig(@JsonProperty("testProp") val testProp: String) : Configuration()
