package to.dev.example

import io.dropwizard.Application
import io.dropwizard.setup.Environment
import to.dev.example.configuration.AppConfig


class App : Application<AppConfig>() {

    companion object {
        @JvmStatic fun main(args : Array<String>) = App().run(*args)
    }

    override fun run(config: AppConfig, env: Environment) { }
}
